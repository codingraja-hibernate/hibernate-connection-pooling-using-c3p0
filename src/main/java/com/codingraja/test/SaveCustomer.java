package com.codingraja.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Customer;

public class SaveCustomer {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure();
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		/*Before executing this application, fist open your mysql console
		and execute query "SHOW STATUS LIKE 'CON%'" it will display
		total no of opened connections*/
		
		Customer customer = new Customer("Sandeep", "Kumar", "support@codingraja.com", 9742900696L);
		
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(customer);
		transaction.commit();
		session.close();
		
		System.out.println("Execute on MySql Console- SHOW STATUS LIKE 'CON%';");
	}

}
