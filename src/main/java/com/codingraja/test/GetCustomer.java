package com.codingraja.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Customer;

public class GetCustomer {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure();
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		/*Before executing this application, fist open your mysql console
		and execute query "SHOW STATUS LIKE 'CON%'" it will display
		total no of opened connections*/
		
		Session session = factory.openSession();
		Customer customer = session.get(Customer.class, 1L);
		session.close();
		
		System.out.println("Customer Name: "+customer.getFirstName());
		
		System.out.println("Execute on MySql Console- SHOW STATUS LIKE 'CON%';");		
	}

}
